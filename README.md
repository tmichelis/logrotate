# Ansible Role: logrotate

This role manages logrotate

## Requirements

|Name |Version |
|:----|:-------|
|None | n.a.   |


## Role Variables
Available variables are listed below with their default values
  # The default variables are common for most distributions
    logrotate_defaults:
      package:
        - logrotate
      config_file: '/etc/logrotate.conf'
      config_dir: '/etc/logrotate.d'
      options:
        - weekly
        - rotate 4
        - compress
        - datext

    # This is for additonal packages, like gzip, bz2 or xz
    optional_packages:
        - gzip
This configures logrotate with the defaults for Distributions of the RedHat family, except, that compression was added by default.


## Dependencies

|Name |Version |
|:----|:-------|
|None | n.a.   |



## Example Playbook

Including an example of how to use your role (for instance, with variables
passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: logrotate, optional_packages: ['bzip2', 'xz'] }


## License

BSD
