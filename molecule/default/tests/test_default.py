import os
import pytest
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_hosts_file(host):
    f = host.file('/etc/hosts')

    assert f.exists
    assert f.user == 'root'
    assert f.group == 'root'


@pytest.mark.parametrize("path", [
    '/etc/logrotate.conf',
    '/etc/logrotate.d',
    '/etc/logrotate.d/httpd',
    '/etc/logrotate.d/wtmp'
])
def test_logrotate_files(host, path):
    f = host.file(path)
    assert f.exists
    assert f.user == 'root'
    assert f.group == 'root'
    if path == '/etc/logrotate.d':
        assert f.is_directory
        assert f.mode == 0o755
    else:
        assert f.is_file
        assert f.mode == 0o444

    if path == '/etc/logrotate.d/httpd':
        assert f.contains('postrotate')
        assert f.contains('/dev/null')
        assert f.contains('missingok')
